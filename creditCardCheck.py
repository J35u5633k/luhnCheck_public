def luhn(input):
    digits = [int(c) for c in input if c.isdigit()]
    checksum = digits.pop()
    digits.reverse()
    doubled = [2*d for d in digits[0::2]]
    total = sum(d-9 if d > 9 else d for d in doubled) + sum(digits[1::2])
    return (total * 9) % 10 == checksum

import re
text = open('list.txt').read()

regex = re.compile(r"[^\d]([\d]{16})[^\d]", re.MULTILINE)
listOfMatches = regex.findall(text)
print len(listOfMatches)
for canidant in listOfMatches:
	canidant = re.sub("[^0-9]", "", canidant)
	if len(canidant) > 10:
		print canidant + ":", luhn(canidant)